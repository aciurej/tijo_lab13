package pl.edu.pwsztar;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ShoppingCartTest {

    @Test
    void addProductTest() {
        ShoppingCart shoppingCart = new ShoppingCart();

        assertTrue(shoppingCart.addProducts("Jajka",10,10));
        assertTrue(shoppingCart.addProducts("Jajka",10,149));
        assertFalse(shoppingCart.addProducts("Mieso", 10, 501));
        assertFalse(shoppingCart.addProducts("Jajka",15,10));
        assertFalse(shoppingCart.addProducts("Woda",0,10));
    }

    @Test
    void deleteProductTest(){
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Mieso", 10, 343);
        shoppingCart.addProducts("Jajka",15,100);

        assertFalse(shoppingCart.deleteProducts("Mieso",344));
        assertTrue(shoppingCart.deleteProducts("Jajka",3));
        assertTrue(shoppingCart.deleteProducts("Mieso",343));

    }

    @Test
    void getQuantityProductTest(){
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Mieso", 10, 6);
        shoppingCart.addProducts("Jajka",15,159);

        assertEquals(6,shoppingCart.getQuantityOfProduct("Mieso"));
        assertEquals(159,shoppingCart.getQuantityOfProduct("Jajka"));

        shoppingCart.deleteProducts("Mieso",3);

        assertEquals(3,shoppingCart.getQuantityOfProduct("Mieso"));
        assertEquals(-1,shoppingCart.getQuantityOfProduct("Cukier"));

    }

    @Test
    void getSumProductsPricesTest(){
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Mieso", 10, 5);
        shoppingCart.addProducts("Mleko", 10,5);

        assertEquals(100,shoppingCart.getSumProductsPrices());
        shoppingCart.addProducts("Cukier", 50,2);
        assertEquals(200,shoppingCart.getSumProductsPrices());
        shoppingCart.deleteProducts("Cukier",2);
        assertEquals(100,shoppingCart.getSumProductsPrices());
    }

    @Test
    void getProductPriceTest(){
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Mieso", 10, 15);
        shoppingCart.addProducts("Mleko", 15,30);

        assertEquals(10,shoppingCart.getProductPrice("Mieso"));
        assertEquals(15,shoppingCart.getProductPrice("Mleko"));
        assertEquals(-1,shoppingCart.getProductPrice("Cukier"));
    }

    @Test
    void getProductsNames(){
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Mieso", 10, 15);
        shoppingCart.addProducts("Mleko", 15,30);
        shoppingCart.addProducts("Cukier", 10, 10);

        List<String> productNames = new ArrayList<>();
        productNames.add("Mieso");
        productNames.add("Mleko");
        productNames.add("Cukier");

        assertEquals(productNames,shoppingCart.getProductsNames());
        shoppingCart.deleteProducts("Mleko",30);
        productNames.removeIf(product -> product.equals("Mleko"));
        assertEquals(productNames,shoppingCart.getProductsNames());
    }
}

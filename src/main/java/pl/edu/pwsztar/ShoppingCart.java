package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ShoppingCart implements ShoppingCartOperation {

    private List<Product> products = new ArrayList<>();

    public boolean addProducts(String productName, int price, int quantity) {
        if(price<=0 || quantity<=0 || sumQuantityProductsInCart()+quantity > PRODUCTS_LIMIT)
        {
            return false;
        }
        if(isProductInCart(productName))
        {
            for(Product product: products){
                if(product.getProductName().equals(productName) && product.getPrice() == price) {
                    product.setQuantity(product.getQuantity()+quantity);
                    return true;
                }
            }
        }else{
            products.add(new Product(productName,price,quantity));
            return true;
        }
        return false;
    }

    public boolean deleteProducts(String productName, int quantity) {
        if(!isProductInCart(productName) || quantity<=0){
            return false;
        }else{
            for(Product product: products){
                if(product.getProductName().equals(productName) && product.getQuantity()>quantity) {
                    product.setQuantity(product.getQuantity() - quantity);
                    return true;
                }else if(product.getProductName().equals(productName) && product.getQuantity()==quantity){
                    products.remove(product);
                    return true;
                }
            }
        }
        return false;
    }

    public int getQuantityOfProduct(String productName) {
        if(!isProductInCart(productName)){
            return -1;
        }else {
            return products.stream().filter(product -> product.getProductName().equals(productName)).findFirst().get().getQuantity();
        }
    }

    public int getSumProductsPrices() {
        return products.stream().mapToInt(product -> product.getPrice() * product.getQuantity()).sum();
    }

    public int getProductPrice(String productName) {
        if(!isProductInCart(productName)){
            return -1;
        }else{
            return products.stream().filter(product -> product.getProductName().equals(productName)).findFirst().get().getPrice();
        }
    }

    public List<String> getProductsNames() {
        return products.stream().map(product -> product.getProductName()).collect(Collectors.toList());
    }

    private int sumQuantityProductsInCart(){
        return products.stream().mapToInt(Product::getQuantity).sum();
    }

    private boolean isProductInCart(String productName){
        return products.stream().anyMatch(product -> product.getProductName().equals(productName));
    }
}
